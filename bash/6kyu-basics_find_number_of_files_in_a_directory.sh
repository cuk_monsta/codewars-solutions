# Your task is to write a script that finds the number of type 'f' files in a
# given directory (argument 1) to stdout.

# If there is no arguments, print "Nothing to find".

# My Answer
#!/bin/bash

if [[ -d $* ]]; then
  num_files="$(ls $* | grep f | wc -l)"
  echo "There are $num_files files in /home/codewarrior/shell/$*"
elif [[ -z $* ]]; then
  echo "Nothing to find"
else
  echo "Directory not found"
fi

# Best Answer
#!/bin/bash

if [ -z $1 ] ; then
  echo "Nothing to find"
  exit
fi

if [ ! -d $1 ] ; then
  echo "Directory not found"
  exit
fi

echo "There are $(find $1 -type f | wc -l) files in $(pwd)/$1"
