# It's pretty straightforward, your goal is to create a function that removes
# the first and last characters of a string. You're given one parameter, the
# original string. You don't have to worry with strings with less than two
# characters.

# My Answer
function removeChar() {
  string=$1
  string2=${string#"${string:0:1}"}
  string2=${string2%"${string2:(-1)}"}
  echo $string2
}
removeChar $1

# Best Answer
arg=$1
echo "${arg:1:-1}"
