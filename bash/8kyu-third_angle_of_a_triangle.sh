# You are given two angles (in degrees) of a triangle.

# Write a function to return the 3rd.

# Note: Only positive integers will be tested.

# My Answer
a=$1
b=$2

echo $((180 - $a - $b))

# Best Answer
#!/usr/bin/env bash

getAngle ()
{
    local -- angleA=$1
    local -- angleB=$2
    local -- totalDegrees=180

    return $(( $totalDegrees - ($angleA + $angleB) ))
}

getAngle $1 $2
echo $?
