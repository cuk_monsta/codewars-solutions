# Consider integer numbers from 0 to n-1 written down along the circle in such a way that the
# distance between any two neighbouring numbers is equal (note that 0 and n-1 are neighbouring
# too).

# Given n and firstNumber / first_number, find the number which is written in the radially
# opposite position to firstNumber.

# Example

# For n=10 and firstNumber=2, the output should be:

# run_shell(args: [n, first_number]) == 7

# Input/Output

# [input] integer n, a positive even integer | Constraints 4 <= n <= 1000.

# [input] integer firstNumber / first_number | Constraints 0 <= firstNumber <= n-1

# [output] an integer

# My Answer
n=$1
first_number=$2

condition=$(($n / 2))

if [ $first_number -ge $condition ]
then
  result=$(($first_number - $condition))
  echo $result
else
  result=$(($first_number + $condition))
  echo $result
fi

# Best Answer
echo $((($2 + $1 / 2) % $1))
