# Take an integer n (n >= 0) and a digit d (0 <= d <= 9) as an integer. Square all numbers k
# (0 <= k <= n) between 0 and n. Count the numbers of digits d used in the writing of all the
# k**2. Call nb_dig (or nbDig or ...) the function taking n and d as parameters and returning
# this count.

# Examples

# n = 10, d = 1, the k*k are 0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100.
# We are using the digit 1 in 1, 16, 81 and 100, the total count is then 4.

# Note that 121 has twice the digit 1.

# My Answer (I need to continue with this kata, I got run time error all times)
#!/bin/bash
nbDig() {

    cont=0

    for num in $(seq 0 $1)
    do
        square=$(($num ** 2))
        plus=$(echo $square | grep -o $2 | wc -l)

        cont=$(($cont + $plus))
    done

    echo $cont
}
nbDig $1 $2

# My Answer (Alternative way)
#!/bin/bash
nbDig() {

    for num in $(seq 0 $1)
    do
        echo "$num^2" | bc -l | grep -o $2 | wc -l
    done > bubu.txt

    echo cat bubu.txt | paste -sd+ bubu.txt | bc
}
nbDig $1 $2

# Best Answer

