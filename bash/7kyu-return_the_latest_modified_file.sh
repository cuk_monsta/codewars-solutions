# Write a script which return the latest modified file.

#For example, if the following files are in the current directory:

# poem.txt
# article.txt
# quotes.txt

# And the latest edited file was article.txt then the following should be
# returned:

# article.txt

# My Answer
ls -tp | grep -v /$* | head -1

# Best Answer
ls -t | head -1
