"""
Timmy's quiet and calm work has been suddenly stopped by his project manager (let's call him boss)
yelling:

- Who named these classes?! Class MyClass? It's ridiculous! I want you to change it to UsefulClass!

Tim sighed, he already knew it's gonna be a long day.

Few hours later, boss came again:

- Much better - he said - but now I want to change that class name to SecondUsefulClass.

And went off. Although Timmy had no idea why changing name is so important for his boss,
he realized, that it's not the end, so he turned to you, his guru, to help him and asked you to
prepare some function, which could change name of given class.

NOTE: Proposed function should allow only names with alphanumeric chars (upper & lower letters plus
      ciphers), but starting only with upper case letter. In other case it should raise an
      exception.

DISCLAIMER: There are obviously betters way to check class name than in example cases, but let's
            stick with that, that Timmy yet has to learn them.
"""

# My Answer
def class_name_changer(cls, new_name):
    if not new_name.isalnum() or new_name[0].islower() or new_name[0].isnumeric():
        raise NameError("Invalid class name!")
    else:
        cls.__name__ = new_name

# Best Answer
def class_name_changer(cls, new_name):
    assert new_name[0].isupper() and new_name.isalnum()
    cls.__name__ = new_name
