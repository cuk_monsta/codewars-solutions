"""
Given two arrays a and b write a function comp(a, b) (compSame(a, b) in Clojure) that checks
whether the two arrays have the same elements, with the same multiplicities. Same means, here,
that the elements in b are the elements in a squared, regardless of the order.

Examples

Valid arrays

a = [121, 144, 19, 161, 19, 144, 19, 11]
b = [121, 14641, 20736, 361, 25921, 361, 20736, 361]

Invalid arrays

a = [121, 144, 19, 161, 19, 144, 19, 11]
b = [132, 14641, 20736, 361, 25921, 361, 20736, 361]

Remarks

  - a or b might be [] (all languages except R or Shell)
  - a or b might be nil, null, None or Nothing (except Haskell, Elixir, C++, Rust, R, Shell or PureScript)
  - If a or b are nil, null or None, the problem doesn't make sense so return False.

Note for C

The two arrays have the same size (>0) given as parameter in function comp.
"""

# My Answer
def comp(array1, array2):
    try:
        return sorted([int(elem**2) for elem in array1]) == sorted(array2)
    except:
        return False

# Best Answer
def comp(array1, array2):
    try:
        return sorted([i ** 2 for i in array1]) == sorted(array2)
    except:
        return False
