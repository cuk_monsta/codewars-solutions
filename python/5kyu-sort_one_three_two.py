"""
Sort these integers for me...

By name!!!

Do it now!!!

Input Data
 Range is 0-999
 There may be duplicates
 The array may be empty
"""

# My Answer
one = [ "", "one ", "two ", "three ", "four ",
        "five ", "six ", "seven ", "eight ",
        "nine ", "ten ", "eleven ", "twelve ",
        "thirteen ", "fourteen ", "fifteen ",
        "sixteen ", "seventeen ", "eighteen ",
        "nineteen "]

ten = [ "", "", "twenty ", "thirty ", "forty ",
        "fifty ", "sixty ", "seventy ", "eighty ",
        "ninety "]

def numToWords(n, s):
    str = ""

    if (n > 19):
        str += ten[n // 10] + one[n % 10]
    else:
        str += one[n]

    if (n):
        str += s

    return str


def convertToWords(n):
    out = ""

    out += numToWords((n // 10000000),
                            "crore ")

    out += numToWords(((n // 100000) % 100),
                                   "lakh ")

    out += numToWords(((n // 1000) % 100),
                             "thousand ")

    out += numToWords(((n // 100) % 10),
                            "hundred ")

    if (n > 100 and n % 100):
        out += "and "

    out += numToWords((n % 100), "")

    return out

def sort_by_name(arr):
    arr_txt = [convertToWords(num) for num in arr]
    my_dict_result = []

    for key, value in zip(arr_txt, arr):
        my_dict_result.append((key, value))

    order_result = sorted(my_dict_result)
    my_dict_result = sorted(my_dict_result)

    result = [my_dict_result[i][1] for i in range(0, len(my_dict_result))]

    if not arr:
       return []
    elif result[0] == 0:
        first_elem = 0
        result
        return result[1:] + [first_elem]

    return result

# Best Answer
def int_to_word(num):
    d = { 0 : 'zero', 1 : 'one', 2 : 'two', 3 : 'three', 4 : 'four', 5 : 'five',
          6 : 'six', 7 : 'seven', 8 : 'eight', 9 : 'nine', 10 : 'ten',
          11 : 'eleven', 12 : 'twelve', 13 : 'thirteen', 14 : 'fourteen',
          15 : 'fifteen', 16 : 'sixteen', 17 : 'seventeen', 18 : 'eighteen',
          19 : 'nineteen', 20 : 'twenty',
          30 : 'thirty', 40 : 'forty', 50 : 'fifty', 60 : 'sixty',
          70 : 'seventy', 80 : 'eighty', 90 : 'ninety' }

    assert(0 <= num)

    if (num < 20):
        return d[num]

    if (num < 100):
        if num % 10 == 0: return d[num]
        else: return d[num // 10 * 10] + '-' + d[num % 10]

    if (num < 1000):
        if num % 100 == 0: return d[num // 100] + ' hundred'
        else: return d[num // 100] + ' hundred and ' + int_to_word(num % 100)

def sort_by_name(arr):
    return sorted(arr, key=int_to_word)

# Tests
Test.assert_equals(sort_by_name([8, 8, 9, 9, 10, 10]), [8, 8, 9, 9, 10, 10])
Test.assert_equals(sort_by_name([1, 2, 3, 4]), [4, 1, 3, 2])
Test.assert_equals(sort_by_name([9, 99, 999]), [9, 999, 99])
