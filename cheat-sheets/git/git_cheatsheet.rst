***************
Git Cheat Sheet
***************

This is a guide to using Git correctly, we will show you several Git commands
for all situations.

Know the status of the repository
*********************************

Status of the local repository
------------------------------

.. code-block:: bash

    $ git status

Get a list of the files of each commit
--------------------------------------

.. code-block:: bash

    $ git log --decorate --pretty=oneline --reverse --name-status
    $ git log --decorate --reverse --name-status # this list shows dates, authors, etc...
    $ git log --reverse --name-status # to get a complete list with as much information as possible

.. code-block:: bash

    $ git rev-list --simplify-by-decoration -2 HEAD # Another option

Obtain a list of the branches that a repository contains (local and remote branches)
------------------------------------------------------------------------------------

.. code-block:: bash

    $ git branch -a

Get a list of the files that Git has already registered from a repository
-------------------------------------------------------------------------

.. code-block:: bash

    $ git ls-tree --full-tree -r --name-only HEAD

Get a list of the files of a particular branch
----------------------------------------------

.. code-block:: bash

    $ git ls-tree -r <branch-name> --name-only

Get a list of all the files that have existed
---------------------------------------------

.. code-block:: bash

    $ git log --pretty=format: --name-only --diff-filter=A | sort -u

Upload a file to a repository
**************************

Add a file to staging area
--------------------------

To add a single file:

.. code-block:: bash

    $ git add <filename>

To add all modified files:

.. code-block:: bash

    $ git add *

To add a part of a file:

.. code-block:: bash

    $ git add -p <filename>

    $ git add -p * # Add parts of all files.

Commit your files
-----------------

To do the first commit:

.. code-block:: bash

    $ git commit -m "message you want to write"

To fixup a commit before the merge requests:

.. code-block:: bash

    $ git commit --fixup=<hash-commit-you-want-to-fix>

Push your changes in the repository
-----------------------------------

If you don't use the `git rebase`, you can use:

.. code-block:: bash

    $ git push origin <branch-to-merge>

If you have used `git rebase`, you must use:

.. code-block:: bash

    $ git push -f origin <branch-to-merge>

Create a Merge Requests in GitLab
-------------------------------

Once you have done the `git push` open in a browser `GitLab`.
You will appreciate that at the top of the screen you have the option
to create a `Merge Requests`.

You should set the `WIP status` in the title (you will see an option to do
that), and you should follow the following steps:

- Write a description if you consider that it will be necessary.
- Assign the merge requests to other person in the project.
- Set a label for the merge requests.
- Mark the option delete source branch when you complete the merge requests.
- Submit your merge requests.

Fixup your Commit in a Merge Requests
-------------------------------------

Imagine you have an open merge request, pushed your commit, but, after the
review, you must modify your code.

Once you have your changes ready locally, you can push another commit
to the merge request, but that is not the right way.

You must send a `fixup commit`, to follow in this way follow these commands:

.. code-block:: bash

    $ git add <files>

    $ git commit --fixup=<hash-original-commit>

    $ git push origin <branch-to-merge>

Squash your fixups in one commit
--------------------------------

Before finalizing the merge request we must perform a squash of our fixups
to keep a clean history.

.. code-block:: bash

    # If you only have to do the squash

    $ git rebase -i --autosquash upstream/master <branch-to-merge>

    # You will see how an editor opens in the terminal, you just have to close
    # the editor without modifying anything with `Ctrl-X`

    $ git push -f origin <branch-to-merge>

    # If you have to add some changes (fixup) and squash your fixups

    $ git add <files>

    $ git commit --fixup=<hash-original-commit>

    $ git rebase -i --autosquash upstream/master <branch-to-merge>

    # You will see how an editor opens in the terminal, you just have to close
    # the editor without modifying anything with `Ctrl-X`

    $ git push -f origin <branch-to-merge>

After the `git push`, you should go to `GitLab` and perform the merge
the merge request.

Git Revert Section (Pending to Do)
----------------------------------

Squash a fixups with revert commits
-----------------------------------

If you want to merge a merge request into upstream but you sent a revert among
the fixups or other option you have to follow this code:

.. code-block:: bash

    # If you only have to do the squash

    $ git rebase -i upstream/master <branch-to-merge>

    # You will see how an editor opens in the terminal, you just have to close
    # the editor without modifying anything with `Ctrl-X`

    $ git push -f origin <branch-to-merge>

    # If you have to add some changes (fixup) and squash your fixups

    $ git add <files>

    $ git commit --fixup=<hash-original-commit>

    $ git rebase -i upstream/master <branch-to-merge>

    # You will see how an editor opens in the terminal, you just have to close
    # the editor without modifying anything with `Ctrl-X`

    $ git push -f origin <branch-to-merge>

Your branch to merge is n commits behind the source branch
----------------------------------------------------------

Imagine you have an open merge request, but while you were doing some changes
in your code your teammade has made a merge of a merge request.

Now, your branch is 1 commit behind the source branch, when you want to merge
your changes your must to do a `rebase` of your branch after you pull the
new changes in your branches.

Follow this code to fix the problem:

.. code-block:: bash

    $ git fetch --all

    $ git rebase -i upstream/master <branch-to-merge>

    $ git push -f origin <branch-to-merge>

Include more examples...