-- #Greetings Grasshopper!# Using only SQL, write a query that returns all
-- rows in the custid, custname and custstate columns from the customers
-- table.

-- My Answer
SELECT custid, custname, custstate
FROM customers

-- Best Answer
SELECT custid, custname, custstate FROM customers;
