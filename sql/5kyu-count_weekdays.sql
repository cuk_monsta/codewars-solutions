-- You need to create a function that calculates the number of weekdays
-- (Monday through Friday) between two dates inclusively.

-- The function should be named weekdays accept two arguments of type DATE
-- and return an INTEGER value.

-- The order of arguments shouldn't matter. To illustrate both of the
-- following queries, it should produce the same result.

-- My Answer
CREATE FUNCTION weekdays(date1 date, date2 date)
RETURNS integer AS
$$
BEGIN
  RETURN (SELECT COUNT(*) AS weekdays
  FROM generate_series(LEAST(date1::date, date2::date), GREATEST(date1::date, date2::date), '1 day'::interval) AS tmp_table
  WHERE EXTRACT('isodow' FROM tmp_table) < 6);
END;
$$ LANGUAGE plpgsql;

-- Best Answer
create or replace function weekdays(_start date, _finish date)
returns integer
as
$$

  select count(*) filter (where extract(dow from g.d) between 1 and 5)::int as weekdays
  from generate_series(least(_start, _finish), greatest(_start, _finish), interval '1 day') as g(d);

$$ language sql;
