-- You have access to a table of monsters as follows:

-- monsters table schema
--  id
--  name
--  legs
--  arms
--  characteristics

-- Your task is to turn the numeric columns (arms, legs) into equivalent
-- hexadecimal values.

-- output table schema
--  legs
--  arms

-- NOTE: Try with to_hex() function

-- My Answer
SELECT to_hex(legs) as legs, to_hex(arms) as arms
FROM monsters

-- Best Answer
SELECT TO_HEX(legs) legs, TO_HEX(arms) arms FROM monsters
