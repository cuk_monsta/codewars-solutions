-- You have access to a table of monsters as follows:

-- monsters table schema
--  id
--  name
--  legs
--  arms
--  characteristics

-- Your task is to make a new table where each column should contain the
-- length of the string in the column to its right (last column should contain
-- length of string in the first column). Remember some column values are
-- not currently strings. Column order and titles should remain unchanged:

-- output table schema
--  id
--  name
--  legs
--  arms
--  characteristics

-- My Answer
SELECT LENGTH(name) AS id,
       LENGTH(CAST(legs as name)) AS name,
       LENGTH(CAST(arms as name)) AS legs,
       LENGTH(characteristics) AS arms,
       LENGTH(CAST(id as name)) AS characteristics
FROM monsters

-- Best Answer
SELECT
  length(name) AS id,
  length(legs::text) AS name,
  length(arms::text) AS legs,
  length(characteristics) AS arms,
  length(id::text) AS characteristics
FROM
  monsters
