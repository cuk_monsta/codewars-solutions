-- Given three integers a, b, c return the largest number obtained after
-- inserting the following operators and brackets +, *, ().

-- NOTES:
--  The numbers are always positive.
--  The numbers are in the range 1 <= a, b, c <= 10
--  You can use the same operation more than once.
--  It is not necessary to place all the signs and brackets.
--  Repetition in numbers may occur.
--  You cannot swap the operands, for instance, in the given example you can't
--  get expression (1 + 3) * 2 = 8.

-- My Answer
SELECT GREATEST(
        a * (b + c),
        a * b * c,
        a + b * c,
        (a + b) * c,
        a + b + c) AS res
FROM expression_matter;

-- Best Answer
SELECT GREATEST( a+b+c, (a+b)*c, a*(b+c), a*b*c )
AS res FROM expression_matter;
