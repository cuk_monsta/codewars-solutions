-- Given a posts table that contains a created_at timestamp column write a
-- query that returns date (without time component), a number of posts for a
-- given date and a running (cumulative) total number of posts up until
-- a given date. The resulting set should be ordered chronologically by date.

-- The resulting set should look similar to the following:

--  date       | count | total
--  2017-01-26 |   20  |   20
--  2017-01-27 |   17  |   37

-- date - (DATE)
-- count - (INT)
-- total - (INT)

-- My Answer
WITH q_count AS (
  SELECT p.created_at::date AS date,
         COUNT(p.title) AS count
  FROM posts AS p
  GROUP BY date
  ORDER BY date
)

SELECT q_count.date, q_count.count,
       SUM(q_count.count) OVER(ORDER BY q_count.date)::integer AS total
FROM q_count

-- Best Answer
SELECT
  CREATED_AT::DATE AS DATE,
  COUNT(CREATED_AT) AS COUNT,
  SUM(COUNT(CREATED_AT)) OVER (ORDER BY CREATED_AT::DATE ROWS UNBOUNDED PRECEDING)::INT AS TOTAL
FROM
  POSTS
GROUP BY
  CREATED_AT::DATE
