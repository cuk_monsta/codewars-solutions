-- For this challenge you need to create a basic increment function which
-- increments on the age field of the peoples table.

-- The function should be called increment, it needs to take 1 integer
-- and increment that number by 1.

-- You may query the people table while testing but the query must only
-- contain the function on your final submit.

-- people table schema
--  id
--  name
--  age

-- My Answer
CREATE FUNCTION increment(age integer) RETURNS integer AS $$
  BEGIN
      RETURN age + 1;
  END;
$$ LANGUAGE plpgsql;

-- Best Answer
CREATE FUNCTION increment(i integer) RETURNS integer
AS 'select $1 + 1;'
LANGUAGE sql;
