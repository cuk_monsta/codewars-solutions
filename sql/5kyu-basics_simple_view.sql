-- For this challenge you need to create a VIEW. This VIEW is used by a sales
-- store to give out vouches to members who have spent over 1000$ in
-- departments that have brought in more than 10000$ total ordered by the
-- members id. The VIEW must be called members_approved_for_voucher then
-- you must create a SELECT query using the view.

-- departments table schema
--  id
--  name

-- products table schema
--  id
--  name
--  price

-- members table schema
--  id
--  name
--  number
--  email
--  city
--  street_name
--  street_address

-- sales table schema
--  id
--  department_id
--  product_id
--  member_id
--  transaction_date

-- resultant table schema
--  id
--  name
--  email
--  total_spending

-- My Answer
CREATE VIEW members_approved_for_voucher AS
  WITH q_table AS (
    SELECT s.department_id, SUM(p.price)
    FROM sales AS s INNER JOIN products AS p
      ON s.product_id = p.id
    GROUP BY s.department_id
    HAVING SUM(p.price) > 10000
  )

SELECT s.member_id AS id, m.name, m.email, SUM(p.price) AS total_spending
FROM sales AS s LEFT JOIN products AS p
  ON s.product_id = p.id
LEFT JOIN members AS m
  ON s.member_id = m.id
WHERE s.department_id IN (SELECT department_id FROM q_table)
GROUP BY (s.member_id, m.name, m.email)
HAVING SUM(p.price) > 1000
ORDER BY s.member_id;

SELECT *
FROM members_approved_for_voucher

-- Best Answer
CREATE VIEW members_approved_for_voucher AS
SELECT m.id, m.name, m.email, SUM(p.price) AS total_spending
FROM members m
INNER JOIN sales s ON s.member_id = m.id
INNER JOIN products p ON p.id = s.product_id
WHERE s.department_id IN (
  SELECT s2.department_id
  FROM sales s2
  INNER JOIN products p2 ON p2.id = s2.product_id
  GROUP BY s2.department_id
  HAVING SUM(p2.price) > 10000
)
GROUP BY m.id, m.name, m.email
HAVING SUM(p.price) > 1000
ORDER BY m.id;

SELECT * FROM members_approved_for_voucher;
