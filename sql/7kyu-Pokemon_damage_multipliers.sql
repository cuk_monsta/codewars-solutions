-- You have arrived at the Celadon Gym to battle Erika for the Rainbow Badge.

-- She will be using Grass-type Pokemon. Any fire pokemon you have will be
-- strong against grass, but your water types will be weakened. The
-- multipliers table within your Pokedex will take care of that.

-- Using the following tables, return the pokemon_name, modifiedstrength and
-- element of the Pokemon whose strength, after taking these changes into
-- account, is greater than or equal to 40, ordered from strongest to
-- weakest.

-- pokemon table schema
--  id
--  pokemon_name
--  element_id
--  str

-- multipliers table schema
--  id
--  element
--  multiplier

-- My Answer
SELECT pokemon_name, pokemon.str * multipliers.multiplier AS modifiedstrength, multipliers.element
FROM pokemon INNER JOIN multipliers
ON pokemon.element_id = multipliers.id
WHERE pokemon.str * multipliers.multiplier >= 40
ORDER BY modifiedstrength DESC

-- Best Answer
SELECT
  p.pokemon_name,
  p.str * m.multiplier AS modifiedStrength,
  m.element
FROM pokemon AS p
JOIN multipliers AS m ON m.id = p.element_id
WHERE (p.str * m.multiplier) >= 40
ORDER BY 2 DESC;
