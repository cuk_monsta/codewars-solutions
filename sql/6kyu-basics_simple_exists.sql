-- For this challenge you need to create a SELECT statement, this SELECT
-- statement will use an EXISTS to check whether a department has had
-- a sale with a price over 98.00$.

-- departments table schema
--  id
--  name

-- sales table schema
--  id
--  deparment_id (department foreign key)
--  name
--  price
--  card_name
--  card_number
--  transaction_date

-- resultant table schema
--  id
--  name

-- My Answer
SELECT id, name
FROM departments
WHERE
  EXISTS( SELECT price
          FROM sales
          WHERE price > 98 AND departments.id = sales.department_id)

-- Best Answer
SELECT d.*
FROM departments d
WHERE EXISTS (SELECT 1 FROM sales s WHERE s.price>98 and s.department_id=d.id);
