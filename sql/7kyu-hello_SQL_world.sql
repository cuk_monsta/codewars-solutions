-- Hello SQL!
-- Return a table with a single column named Greeting with the phrase hello
-- world!.

-- My Answer
SELECT 'hello world!' AS "Greeting"

-- Best Answer
SELECT 'hello world!' as "Greeting"
