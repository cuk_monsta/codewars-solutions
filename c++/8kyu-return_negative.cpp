/*
In this simple assignment you are given a number and have to make it negative. But maybe the number
is already negative?

Example:
makeNegative(1); // return -1
makeNegative(-5); // return -5
makeNegative(0); // return 0

NOTES
- The number can be negative already, in which case no change is required.
- Zero is not checked for any specific sign, negative zeros make no mathematical sense.
/**/

// My Answer
int makeNegative(int num)
{
  if (num > 0) {
    return -1 * num;
    } else if (num < 0) {
      return num;
      } else {
        return 0;
        };
}

// Best Answer
int makeNegative(int num) {
  return num > 0 ? -num : num;
}
