/*
You get an array of numbers, return the sum of all of the positives ones.

Example:

[1, -4, 7, 12] => 1 + 7 + 12 = 20

NOTE: If there is nothing to sum, the sum is default to 0.
/**/

// My Answer
#include <vector>

int positive_sum (const std::vector<int> arr){

  int result = 0;

  for (int i = 0; i < arr.size(); i++) {
    if (arr[i] > 0) {
      result += arr[i];
    }
  }
  return result;
}

// Best Answer
#include <vector>

int positive_sum (const std::vector<int> &arr){
  int sum = 0;
    for (int i = 0; i < arr.size(); i++) {
      if (arr[i] > 0) sum += arr[i];
    }
  return sum;
}
